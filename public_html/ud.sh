#!/bin/sh
# Based on https://mcmackins.org/ud.sh

# Copy to version control
git push

# Clean
find ./ -name "*~" -delete

# Copies all the things to Caleb's web directory
rsync -Purv --delete --chmod=777 ./ csh@bluehome.net:/home/csh/public_html/
